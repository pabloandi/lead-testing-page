window.validate = require('validate.js');

import Form from './classes/Form.js';

let fields = {
    url: null,
    nombre: null,
    email: null,
    telefono: null,
    pais: null,
    mensaje: null,
    l_lead_type: "2"
};

let constraints = {
    url: {
        presence: {
            allowEmpty: false,
            message: "*Este campo es requerido"
        },
        url: {
          allowLocal: true,
          message: "*No es una URL válida"
        }
    },
    nombre: {
        presence: {
            allowEmpty: false,
            message: "*Este campo es requerido"
        }
    },
    telefono: {
        presence: {
            allowEmpty: false,
            message: "*Este campo es requerido"
        }
    }
};


const app = new Vue({
    el: '#app',
    data: {
        form : new Form(fields, constraints, window.validate),
        submitingError: null,
        submitingSuccess: null
    },
    methods: {
        onSubmit(){
            this.form.validateData();
            if(!this.form.errors.any()){
                let prod_server = 'https://iz590xje3m.execute-api.us-west-2.amazonaws.com/prod/leads';
                let dev_server = 'http://localhost:7070/subscribe/formlead';
                let endpoint = (process.env.NODE_ENV === 'production') ? prod_server : dev_server ;

                this.form.post(endpoint)
                .then(data => {
                    console.log('POST OK.');
                    this.submitingSuccess = "Prospecto enviado";
                    this.form.nombre=null;
                    this.form.email=null;
                    this.form.mensaje=null;
                    this.form.telefono=null;
                    this.form.pais=null;

                })
                .catch(errors => {
                    console.log('POST Failed.');
                    this.submitingError = "Hubo un error al enviar la información. Por favor intentelo de nuevo.";
                })
            }
        }
    }
});
